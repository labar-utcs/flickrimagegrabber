import shutil
import os

source = 'pics/'
traindest = 'train/'
testdest = 'test/'
percentTrain = 0.85
cur = os.getcwd() + '/'

os.mkdir(cur + traindest)
os.mkdir(cur + testdest)

files = os.listdir(source)
count = 0
train_count = 0

for f in files:
    count += 1
    if float(train_count)/float(count) > percentTrain:
        shutil.move(source + f, cur + testdest)
    else:
        shutil.move(source + f, cur + traindest)
        train_count += 1